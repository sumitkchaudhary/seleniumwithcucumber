package org.orangehrm.qa.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.orangehrm.qa.qa.utilities.BrowserHandler;

public class LoginPage extends BrowserHandler {
    @FindBy (xpath = "//input[@placeholder='Username']")
    public static WebElement usernameInputBox;
    @FindBy(xpath = "//input[@placeholder='Password']")
    public static WebElement passwordInputBox;
    @FindBy(xpath = "//button[@type='submit']")
    public static WebElement loginButton;
    @FindBy(xpath = "//div[@class='oxd-alert-content oxd-alert-content--error']")
    public static WebElement wrongLoginMessage;

    public LoginPage(WebDriver driver){
        super.driver=driver;
        PageFactory.initElements(driver,this);
    }
    public void validateLoginFunctionality(String username, String password){
        usernameInputBox.sendKeys(username);
        passwordInputBox.sendKeys(password);
        loginButton.click();
    }
    public static String  invalidLoginFunctionality(String username, String password){
        LoginPage lp=new LoginPage(driver);
        lp.validateLoginFunctionality(username, password);
        return wrongLoginMessage.getText();
    }


}
