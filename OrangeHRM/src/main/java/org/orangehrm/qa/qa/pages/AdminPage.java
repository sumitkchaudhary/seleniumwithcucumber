package org.orangehrm.qa.qa.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.orangehrm.qa.qa.utilities.BrowserHandler;

public class AdminPage extends BrowserHandler {
    public AdminPage(WebDriver driver){
        super.driver=driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[normalize-space()='Admin']]")
    private static WebElement adminTab;

    @FindBy(xpath = "//button[normalize-space()='Add']")
    private static WebElement addUserButton;

    @FindBy(xpath = "//div[@class='oxd-select-text oxd-select-text--focus']//div[@class='oxd-select-text-input'][normalize-space()='-- Select --']")
    private static WebElement userRoleDropDown;

    @FindBy(xpath = "//input[@placeholder='Type for hints...']")
    private static WebElement employeeNameInputBox;

    @FindBy(xpath = "//div[@class='oxd-select-text oxd-select-text--focus']//div[@class='oxd-select-text-input'][normalize-space()='-- Select --']")
    private static WebElement statusDropDown;

    private static void navigateAdminTab(){
        adminTab.click();
    }
    public void addNewUser(){
        navigateAdminTab();
        addUserButton.click();

        actionActivities.click(userRoleDropDown).perform();
        actionActivities.keyDown(Keys.ARROW_DOWN);

    }



}
