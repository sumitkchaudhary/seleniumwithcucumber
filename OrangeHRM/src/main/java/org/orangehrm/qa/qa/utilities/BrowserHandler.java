package org.orangehrm.qa.qa.utilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;
import java.util.Properties;

public class BrowserHandler extends MasterControllers {
    public static WebDriver driver;
    public static Properties pr=getPropertyFileValue("ConfigFile.properties");
    public static Actions actionActivities;
    public static void launchBrowser() {
        String browserType = pr.getProperty("browserName");
        if (browserType.contains("chrome")) {
            ChromeOptions options = new ChromeOptions();
            WebDriverManager.chromedriver().setup();
            options.addArguments("--disable-notifications");
            driver = new ChromeDriver(options);
        } else if (browserType.contains("firefox")) {
            FirefoxOptions notification = new FirefoxOptions();
            notification.addPreference("dom.webnotifications.enabled", false);
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver(notification);
        } else if (browserType.contains("edge")) {
            EdgeOptions notification = new EdgeOptions();
            notification.setCapability("--disable-notifications", true);
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver(notification);
        }
        driver.get("https://opensource-demo.orangehrmlive.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(600));
        actionActivities=new Actions(driver);
    }


}
