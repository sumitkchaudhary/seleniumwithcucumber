package org.orangehrm.qa.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.orangehrm.qa.qa.utilities.BrowserHandler;

public class ForgotPasswordPage extends BrowserHandler {

    @FindBy(xpath = "//p[@class='oxd-text oxd-text--p orangehrm-login-forgot-header']")
    public static WebElement forgotPasswordLinker;
    @FindBy(xpath = "//input[@placeholder='Username']")
    public static WebElement usernameInputbox;

    @FindBy(xpath = "//button[@type='submit']")
    public static WebElement resetPasswordButton;

    @FindBy(xpath = "//h6[@class='oxd-text oxd-text--h6 orangehrm-forgot-password-title']")
    public static WebElement restPasswordLinkSentMessage;



    public ForgotPasswordPage(WebDriver driver){
        super.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public static String validateRestPasswordLinkSentMessage(String username){
        new Actions(driver).click(forgotPasswordLinker).perform();

        usernameInputbox.sendKeys(username);
        resetPasswordButton.click();
        return restPasswordLinkSentMessage.getText();
    }


}
