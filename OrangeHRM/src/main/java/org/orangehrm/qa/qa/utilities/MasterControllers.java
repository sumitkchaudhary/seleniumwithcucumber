package org.orangehrm.qa.qa.utilities;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

public class MasterControllers {

    /**
     * getPropertyFileValue method help to get value from property file as respective a key the method require
     * @param fileName so that the method can read the file and load all the properties
     * @return the value.
     */
    public static Properties getPropertyFileValue(String fileName) {
        Properties  pr=new Properties();
        try {
            FileInputStream fis=new FileInputStream(new File(getFileAbsolutePathByName(fileName)));
            pr.load(fis);
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        }
        return pr;
    }

    /**
     * getFileAbsolutePathByName method help to search a file by there name the required parameter is
     * @param fileName
     * @return the file path
     */
    public static String getFileAbsolutePathByName(String fileName){
        String fileAbsolutePath="";
        try{
            Collection<File> files= FileUtils.listFiles(new File(System.getProperty("user.dir")),null, true);
            for(Iterator<File> iterator=files.iterator(); iterator.hasNext();){
                File file=(File)iterator.next();
                if(file.getName().equals(fileName))
                    fileAbsolutePath=file.getAbsolutePath();
            }
        }catch (Exception e){

            e.printStackTrace();
            System.err.println("An unexpected error occured: "+e.getMessage());
        }
        return fileAbsolutePath;
    }

}
