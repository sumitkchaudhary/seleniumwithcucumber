Feature: Check login functionality with all possible scenarios.

  Scenario Outline: Run login functionality with all possible data and validate the behaviour.

    Given Correct credential
    When provide "<username>" and "<password>"
    Then validate the home page will open

    Examples:
    | username  | password  |
    | Admin     | admin123 |


  Scenario Outline: Run login functionality with wrong credential and verify the validation message.

    Given Incorrect credential
    When provide incorrect username "<incorrectUsername>" and incorrect password "<incorrectPassword>"
    Then verify the validation message

    Examples:
      | incorrectUsername  | incorrectPassword  |
      | dsfsdf     | dfsdf |
