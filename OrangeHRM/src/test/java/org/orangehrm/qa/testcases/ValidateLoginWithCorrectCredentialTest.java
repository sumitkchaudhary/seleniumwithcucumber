package org.orangehrm.qa.testcases;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.orangehrm.qa.qa.pages.LoginPage;
import org.orangehrm.qa.qa.utilities.BrowserHandler;

public class ValidateLoginWithCorrectCredentialTest extends BrowserHandler {

    @Given("Correct credential")
    public void correctCredential() {
        launchBrowser();

    }

    @When("provide {string} and {string}")
    public void provideAnd(String username, String password) {
        LoginPage lp=new LoginPage(driver);
        lp.validateLoginFunctionality(username,password);
    }

    @Then("validate the home page will open")
    public void validateTheHomePageWillOpen() {
        String expected="https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index";
        String actual=driver.getCurrentUrl();
        Assert.assertEquals(expected,actual);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        driver.quit();
    }


}
