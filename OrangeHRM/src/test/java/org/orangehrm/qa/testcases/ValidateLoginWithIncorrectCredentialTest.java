package org.orangehrm.qa.testcases;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.orangehrm.qa.qa.pages.LoginPage;
import org.orangehrm.qa.qa.utilities.BrowserHandler;

public class ValidateLoginWithIncorrectCredentialTest extends BrowserHandler {

    @Given("Incorrect credential")
    public void incorrectCredential() {
        launchBrowser();
    }

    String actualValidationMessage;
    @When("provide incorrect username {string} and incorrect password {string}")
    public void provideIncorrectUsernameAndIncorrectPassword(String invalidUsername, String invalidPassword) {
        actualValidationMessage=LoginPage.invalidLoginFunctionality(invalidUsername,invalidPassword);

    }

    @Then("verify the validation message")
    public void verifyTheValidationMessage() {
        String expectedValidationMessage="Invalid credentials";
        Assert.assertEquals(expectedValidationMessage,actualValidationMessage);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        driver.quit();
    }
}
