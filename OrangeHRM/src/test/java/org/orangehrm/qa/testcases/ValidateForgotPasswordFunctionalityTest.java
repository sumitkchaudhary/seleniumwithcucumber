package org.orangehrm.qa.testcases;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.orangehrm.qa.qa.pages.ForgotPasswordPage;
import org.orangehrm.qa.qa.utilities.BrowserHandler;

public class ValidateForgotPasswordFunctionalityTest extends BrowserHandler {
    @Given("launch forgot password page by click forgot password link from login page.")
    public void launchForgotPasswordPageByClickForgotPasswordLinkFromLoginPage() {
        launchBrowser();
    }
    String forgotPasswordSentConfirmationActualMessage;
    @When("user provide correct user name {string} and click rest password button")
    public void userProvideCorrectUserNameAndClickRestPasswordButton(String username) {
        //driver.navigate().to("https://opensource-demo.orangehrmlive.com/web/index.php/auth/requestPasswordResetCode");
        forgotPasswordSentConfirmationActualMessage= ForgotPasswordPage.validateRestPasswordLinkSentMessage(username);
    }

    @Then("system should display sent link message.")
    public void systemShouldDisplaySentLinkMessage() {
        Assert.assertEquals("Reset Password link sent successfully",forgotPasswordSentConfirmationActualMessage);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        driver.quit();
    }
}
