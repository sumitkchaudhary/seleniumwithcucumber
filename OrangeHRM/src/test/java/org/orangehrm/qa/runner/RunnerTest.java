package org.orangehrm.qa.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        publish = false,
        features = {"src/test/resources/features"},
        glue="org.orangehrm.qa.testcases",
        plugin= {"pretty",
                "json:target/cucumber.json"
        }
)
public class RunnerTest {



}
