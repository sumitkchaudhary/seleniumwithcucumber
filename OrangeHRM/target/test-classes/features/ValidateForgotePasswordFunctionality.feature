Feature: Check forgot password functionality.
  Scenario Outline: Validate that when user request rest password then system should sent forgot password link.

    Given launch forgot password page by click forgot password link from login page.
    When user provide correct user name "<username>" and click rest password button
    Then system should display sent link message.

    Examples:
    |username|
    |sumit   |